// Теоретичні питання

// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
// Прототипне наслідування дає можливість використовувати метоти та властивості одного об'єкта в іншому об'єкті.
// Якщо якийсь метод чи властивість не прописані в об'єкті-нащадку, то вони беруться з об'єкта-прототипа,
// при цьому сам об'єкт можна додатково доповнювати/розширювати новими методами аба властивостями. Але прототипне
// наслідування при цьому не є сліпим копіюванням, що спрощує написання коду, дає гнучкість та оптимізацію.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Ключове слово super() у конструкторі класу-нащадка необхідно викликати для того, щоб викликати батьківський
// конструктор. Super() дозволяє визначити батьківський клас та сформувати контекст this для нащадка, який включає
// в себе не тільки свої методи та властивості, а й все те, що є в батьківському класі.

class Employee {
    set name(value) {
        if (value < 2) {
            console.log("Name is too short")
        } else {
            this._name = value
        }
    };

    get name() {
        return this._name
    }

    set age(value) {
        if (value < 18) {
            console.log("Employee is too young")
        } else if (value >= 60) {
            console.log("Employee is too old")
        } else {
            this._age = value
        }
    }

    get age() {
        return this._age
    }

    set salary(value) {
        if (typeof value === 'number') {
            this._salary = value
        }
    }

    get salary() {
        return this._salary
    }

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang = "Ukrainian") {
        super(name, age, salary);
        this.lang = lang
    }

    get salary() {
        return super.salary * 3
    }

    get allInfo() {
        return `Name: ${this.name}, Age: ${this.age} years, Salary: ${this.salary} $, Languages: ${this.lang}`
    }

}

const programmer1 = new Programmer("Roman", 29, 1000, ["Ukrainian", "English", "French"]);

const programmer2 = new Programmer("Oleksandr", 25, 1000);
programmer2.lang = ["Ukrainian", "English"];

console.log(programmer1.allInfo);
console.log(programmer2.allInfo);


